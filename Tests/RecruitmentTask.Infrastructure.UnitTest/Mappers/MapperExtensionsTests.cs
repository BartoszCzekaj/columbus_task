using FluentAssertions;
using NUnit.Framework;
using RecruitmentTask.Core.Model;
using RecruitmentTask.Infrastructure.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using Toolset.Http.Columbus.DTO;

namespace RecruitmentTask.Infrastructure.UnitTest.Mappers
{
    public class MapperExtensionsTests
    {

        [Test]
        public void When_EmployeeDto_Is_Null_Then_Throw_Exception()
        {
            EmployeeDto employeeDto = null;
            Action act = () => employeeDto.ToEmployeeModel();
            act.Should().Throw<NullReferenceException>();
        }

        [Test]
        public void When_EmployeeDto_Links_Is_Null_Then_Links_Should_Be_Empty()
        {
            var employeeDto = new EmployeeDto
            {
                FirstName = "FirstName",
                LastName = "LastName",
                ID = 1,
                JobTitle = "JobTitle"
            };

            var employee = employeeDto.ToEmployeeModel();

            employee.JobTitle.Should().Be(employeeDto.JobTitle);
            employee.LastName.Should().Be(employeeDto.LastName);
            employee.ID.Should().Be(employeeDto.ID);
            employee.FirstName.Should().Be(employeeDto.FirstName);
            employee.Links.Should().BeEmpty();
        }

        [Test]
        public void When_EmployeeDto_Has_No_Links_Then_Links_Should_Be_Empty()
        {
            var employeeDto = new EmployeeDto
            {
                FirstName = "FirstName",
                LastName = "LastName",
                ID = 1,
                JobTitle = "JobTitle",
                Links = Enumerable.Empty<LinkDto>().ToList()
            };

            var employee = employeeDto.ToEmployeeModel();

            employee.JobTitle.Should().Be(employeeDto.JobTitle);
            employee.LastName.Should().Be(employeeDto.LastName);
            employee.ID.Should().Be(employeeDto.ID);
            employee.FirstName.Should().Be(employeeDto.FirstName);
            employee.Links.Should().BeEmpty();
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void When_EmployeeDto_String_Values_Are_Null_Or_Empty_Then_Mapped_Value_Is_Null_Or_Empty(string value)
        {
            var employeeDto = new EmployeeDto
            {
                FirstName = value,
                LastName = value,
                ID = 1,
                JobTitle = value,
                Links = Enumerable.Empty<LinkDto>().ToList()
            };

            var employee = employeeDto.ToEmployeeModel();

            employee.JobTitle.Should().Be(employeeDto.JobTitle);
            employee.LastName.Should().Be(employeeDto.LastName);
            employee.ID.Should().Be(employeeDto.ID);
            employee.FirstName.Should().Be(employeeDto.FirstName);
            employee.Links.Should().BeEmpty();
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void When_Link_Fields_Are_Null_Or_Empty(string linkData)
        {
            var linkDto = new LinkDto
            {
                Href = linkData,
                Method = linkData,
                Rel = linkData
            };

            var link = new Link
            {
                Href = linkData,
                Method = linkData,
                Rel = linkData
            };

            var employeeDto = new EmployeeDto
            {
                FirstName = "FirstName",
                LastName = "LastName",
                ID = 1,
                JobTitle = "JobTitle",
                Links = new List<LinkDto> 
                {
                    linkDto
                }
            };

            var employee = employeeDto.ToEmployeeModel();

            employee.JobTitle.Should().Be(employeeDto.JobTitle);
            employee.LastName.Should().Be(employeeDto.LastName);
            employee.ID.Should().Be(employeeDto.ID);
            employee.FirstName.Should().Be(employeeDto.FirstName);
            employee.Links.Should().Equal(new List<Link>{ link });
        }
    }
}