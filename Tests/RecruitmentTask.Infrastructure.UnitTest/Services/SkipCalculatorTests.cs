﻿using FluentAssertions;
using NUnit.Framework;
using RecruitmentTask.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentTask.Infrastructure.UnitTest.Services
{
    public class SkipCalculatorTests
    {

        [TestCase(-1)]
        [TestCase(0)]
        public void When_Page_Is_Less_Than_Zero(int page)
        {
            var skipCalculator = new SkipCalculator();
            Action act = () => skipCalculator.Calculate(page, 10);
            act.Should().Throw<ArgumentException>();
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void When_Page_Size_Is_Less_Than_Zero(int pageSize)
        {
            var skipCalculator = new SkipCalculator();
            Action act = () => skipCalculator.Calculate(1, pageSize);
            act.Should().Throw<ArgumentException>();
        }

        [TestCase(1, 10, ExpectedResult = 0)]
        [TestCase(1, 20, ExpectedResult = 0)]
        [TestCase(2, 10, ExpectedResult = 10)]
        [TestCase(2, 20, ExpectedResult = 20)]
        [TestCase(40, 20, ExpectedResult = (39*20))]
        [TestCase(1, 1, ExpectedResult = 0)]
        [TestCase(2, 1, ExpectedResult = 1)]
        [TestCase(3, 1, ExpectedResult = 2)]
        public int Calculate_Skip_Value(int page, int pageSize)
        {
            var skipCalculator = new SkipCalculator();
            return skipCalculator.Calculate(page, pageSize);
        }
    }
}
