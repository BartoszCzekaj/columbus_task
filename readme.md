# Columbus Poland recruitment - TECHNICAL TASK

### Preparation for the task

Before you proceed to task please fork the repository.
[How to fork on Bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

### Task description

Your task is to implement the following functionalities based on solution from the repository.

- As a guest you should see a list of all employees. The list must be paginated.
- As a guest you should be able to view the details of each employee
- As a guest you should be able to filter the list of employees by their position. The list should be paginated.

### Solution
- The provided solution contains a project for MVC application based on .NET Framework
- You can add new projects to the solution

### Data
- The source of data for the task is the `REST API` service, to which you received the email address
- Authorization to the site is done using the `ApiKey` header, whose value you also received in the email
- The website has documentation in the form of `Swagger UI`

### Technical requirements
- The solution should be implemented using `ASP.NET MVC`, in such a way that every user action requires a page reload (we do not want to check javascript skills)
- The solution must compile
- Application functionalities must work
- The solution code should be `of the highest production quality`. (We will focus mainly on this aspect when assessing the task.)
- The application should be unit tested

### Visual aspects
- The visual aspects of the application will be ignored during the task evaluation

### Providing solution
- As a solution to the task you should provide us with a link to the repository, where there is a `fork` with the solution of the task or the zip file contianing the solution.

### Help and Questions
- If you have any questions about the task, please contact us by last email.


Good luck