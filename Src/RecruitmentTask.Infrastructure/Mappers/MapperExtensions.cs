﻿using RecruitmentTask.Core.Extensions;
using RecruitmentTask.Core.Model;
using System.Linq;
using Toolset.Http.Columbus.DTO;

namespace RecruitmentTask.Infrastructure.Mappers
{
    internal static class MapperExtensions
    {
        public static Employee ToEmployeeModel(this EmployeeDto employeeDto)
        {
            return new Employee
            {
                FirstName = employeeDto.FirstName,
                LastName = employeeDto.LastName,
                ID = employeeDto.ID,
                JobTitle = employeeDto.JobTitle,
                Links = employeeDto.Links.UseIfNullOrEmpty().Select(x=>x.ToLinkModel()).ToList()
            };
        }

        public static Link ToLinkModel(this LinkDto linkDto)
        {
            return new Link
            {
                Href = linkDto.Href,
                Method = linkDto.Method,
                Rel = linkDto.Rel
            };
        }

        public static EmployeeDetails ToEmployeeDetailsModel(this EmployeeDetailsDto employeeDetailsDto, string firstName, string lastName)
        {
            return new EmployeeDetails
            {
                FirstName = firstName,
                LastName = lastName,
                DayOfEmployment = employeeDetailsDto.DayOfEmployment,
                PhoneNumber = employeeDetailsDto.PhoneNumber,
                JobTitle = employeeDetailsDto.JobTitle,
                Salary = employeeDetailsDto.Salary,
                Email = employeeDetailsDto.Email,
                Links = employeeDetailsDto.Links.UseIfNullOrEmpty().Select(x => x.ToLinkModel()).ToList(),
                Address = employeeDetailsDto.Address.ToAddressModel()
            };
        }

        public static Address ToAddressModel(this AddressDto addressDto)
        {
            return new Address
            {
                Street = addressDto.Street,
                HouseNumber = addressDto.HouseNumber,
                City = addressDto.City
            };
        }
    }
}
