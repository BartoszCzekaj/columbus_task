﻿using Autofac;
using Newtonsoft.Json;
using RecruitmentTask.Core.Options;
using RecruitmentTask.Infrastructure.Services;
using System.Collections.Specialized;

namespace RecruitmentTask.Infrastructure
{
    public static class Extensions
    {
        private const string _sectionName = "App";
        public static ContainerBuilder AddInfrastructure(this ContainerBuilder containerBuilder, NameValueCollection settings)
        {
            var sectionData = settings[_sectionName];
            var model = (AppOptions)JsonConvert.DeserializeObject(sectionData, typeof(AppOptions));
            
            containerBuilder.RegisterInstance(model).SingleInstance();
            containerBuilder.RegisterType<Employees>().As<IEmployees>();
            containerBuilder.RegisterType<SkipCalculator>().As<ISkipCalculator>();
            containerBuilder.RegisterType<Employee>().As<IEmployee>();
            return containerBuilder;
        }
    }
}
