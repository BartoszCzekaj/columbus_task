﻿using RecruitmentTask.Core.Model;
using System.Threading.Tasks;

namespace RecruitmentTask.Infrastructure.Services
{
    public interface IEmployee
    {
        Task<EmployeeDetails> GetDetails(int id, string firstName, string lastName);
    }
}
