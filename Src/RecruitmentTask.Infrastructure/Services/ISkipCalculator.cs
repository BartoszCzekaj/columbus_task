﻿namespace RecruitmentTask.Infrastructure.Services
{
    public interface ISkipCalculator
    {
        int Calculate(int page, int pageSize);
    }
}