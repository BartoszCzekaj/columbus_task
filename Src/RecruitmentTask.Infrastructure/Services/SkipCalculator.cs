﻿using RecruitmentTask.Core.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecruitmentTask.Infrastructure.Services
{
    public class SkipCalculator : ISkipCalculator
    {
        public int Calculate(int page, int pageSize)
        {
            if (page < 1) { throw new ArgumentException(); }
            if (pageSize < 1) { throw new ArgumentException(); }
            return (page - 1) * pageSize;
        }
    }
}
