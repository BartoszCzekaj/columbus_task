﻿using RecruitmentTask.Core.Model;
using RecruitmentTask.Infrastructure.Mappers;
using System.Threading.Tasks;
using Toolset.Http.Columbus.Employees;

namespace RecruitmentTask.Infrastructure.Services
{
    internal class Employee : IEmployee
    {
        private readonly IEmployeeDetails _employeeDetails;
        public Employee(IEmployeeDetails employeeDetails)
        {
            _employeeDetails = employeeDetails;
        }

        public async Task<EmployeeDetails> GetDetails(int id, string firstName, string lastName)
        {
            var employeesDto = await _employeeDetails.Get(id);

            return employeesDto?.ToEmployeeDetailsModel(firstName, lastName);
        }
    }
}
