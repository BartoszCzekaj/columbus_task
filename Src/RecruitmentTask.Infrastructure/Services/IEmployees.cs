﻿using RecruitmentTask.Core.Model;
using System.Threading.Tasks;

namespace RecruitmentTask.Infrastructure.Services
{
    public interface IEmployees
    {
        Task<PageData> GetPage(int page);
        Task<PageData> GetPageByJobTitle(int page, string jobTitle);
    }
}
