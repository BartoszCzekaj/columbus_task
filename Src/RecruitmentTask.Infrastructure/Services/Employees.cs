﻿using RecruitmentTask.Core.Model;
using RecruitmentTask.Core.Options;
using RecruitmentTask.Infrastructure.Mappers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Toolset.Http.Columbus.Employees;

namespace RecruitmentTask.Infrastructure.Services
{
    internal class Employees : IEmployees
    {
        private readonly int _pageSize;
        private readonly IAllEmployees _allEmployees;
        private readonly ISkipCalculator _skipCalculator;

        public Employees(AppOptions appOtions, IAllEmployees allEmployees, ISkipCalculator skipCalculator)
        {
            _pageSize = appOtions.PageSize > 0 
                ? 10
                : appOtions.PageSize;

            _allEmployees = allEmployees;
            _skipCalculator = skipCalculator;
        }

        public async Task<PageData> GetPage(int page)
        {
            var employeesDto = await _allEmployees.Get(_skipCalculator.Calculate(page, _pageSize), _pageSize);
            var jobTitles = await GetAllJobItles();

            return BuildPageData(page
                , string.Empty
                , employeesDto.Take(_pageSize).Select(x => x.ToEmployeeModel())
                , jobTitles);
        }

        public async Task<PageData> GetPageByJobTitle(int page, string jobTitle)
        {
            var employeesDto = await _allEmployees.GetAll();
            var jobTitles = await GetAllJobItles();

            return BuildPageData(page
                , jobTitle
                , employeesDto.Where(x => x.JobTitle == jobTitle).Skip(_skipCalculator.Calculate(page, _pageSize) + 1).Take(_pageSize).Select(x => x.ToEmployeeModel())
                , jobTitles);
        }

        private PageData BuildPageData(int page, string jobTitle, IEnumerable<Core.Model.Employee> employees, IEnumerable<string> jobTitles)
        {
            return new PageData()
            { 
                PageSize = _pageSize,
                JobTitle = jobTitle,
                JobTitles = jobTitles.ToList(),
                Employees = employees.ToList(),
                Current = page
            };
        }

        private async Task<IEnumerable<string>> GetAllJobItles()
        {
            var employeesDto = await _allEmployees.GetAll();

            return employeesDto.Select(x => x.JobTitle).Distinct();
        }
    }
}
