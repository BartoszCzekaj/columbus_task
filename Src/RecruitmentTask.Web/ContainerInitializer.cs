﻿using System.Configuration;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using RecruitmentTask.Infrastructure;
using Toolset.Core;
using Toolset.Http;
using Toolset.Http.Columbus;

namespace RecruitmentTask.Web
{
    public static class ContainerInitializer
    {
        public static void Initialize()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);

            RegisterTypes(builder);

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder
                .AddToolset(ConfigurationManager.AppSettings)
                .AddHttpClient()
                .AddColumbusHttp();

            builder
                .AddInfrastructure(ConfigurationManager.AppSettings);
            //example
            //builder.RegisterType<HelloService>().AsImplementedInterfaces().InstancePerRequest();
        }
    }
}