﻿using System.Threading.Tasks;
using System.Web.Mvc;
using RecruitmentTask.Infrastructure.Services;
using RecruitmentTask.Core.Extensions;

namespace RecruitmentTask.Web.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployees _employees;
        public EmployeesController(IEmployees employees)
        {
            _employees = employees;
        }
        // GET: Employees
        
        public async Task<ActionResult> Index(int? page)
        {
            int pageNumber = page == null ? 1 : (int)page;
            var pageData = await _employees.GetPage(pageNumber);

            return View(pageData);
        }

        [HttpGet]
        public async Task<ActionResult> ByJobItle(int? page, string jobTitle)
        {
            if(!jobTitle.HasValue())
            {
                return Redirect("/Employees/Index");
            }
            int pageNumber = page == null ? 1 : (int)page;
            var pageData = await _employees.GetPageByJobTitle(pageNumber, jobTitle);

            return View("Index", pageData);
        }
    }
}