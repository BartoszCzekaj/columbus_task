﻿using System.Threading.Tasks;
using System.Web.Mvc;
using RecruitmentTask.Infrastructure.Services;

namespace RecruitmentTask.Web.Controllers
{
    public class EmployeeDetailsController : Controller
    {
        private readonly IEmployee _employee;
        public EmployeeDetailsController(IEmployee employee)
        {
            _employee = employee;
        }
        // GET: EmployeeDetails
        public async Task<ActionResult> Index(int id, string firstName, string lastName)
        {
            var details = await _employee.GetDetails(id, firstName, lastName);
            return View(details);
        }
    }
}