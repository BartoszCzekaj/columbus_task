﻿using System.Collections.Generic;

namespace RecruitmentTask.Core.Model
{
    public sealed class Employee
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public List<Link> Links { get; set; }
    }
}
