﻿using System;
using System.Collections.Generic;

namespace RecruitmentTask.Core.Model
{
    public sealed class EmployeeDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DayOfEmployment { get; set; }
        public string PhoneNumber { get; set; }
        public string JobTitle { get; set; }
        public double Salary { get; set; }
        public string Email { get; set; }
        public List<Link> Links { get; set; }
        public Address Address { get; set; }
    }
}
