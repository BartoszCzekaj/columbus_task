﻿using System.Collections.Generic;

namespace RecruitmentTask.Core.Model
{
    public class PageData
    {
        public List<Employee> Employees { get; set; }
        public int Current { get; set; }
        public List<string> JobTitles { get; set; }
        public string JobTitle { get; set; }
        public int PageSize { get; set; }
    }
}
