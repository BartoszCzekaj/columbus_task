﻿namespace RecruitmentTask.Core.Model
{
    public sealed class Link
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public string Method { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var number = obj as Link;

            return (number != null)
                && (Href == number.Href)
                && (Rel == number.Rel)
                && (Method == number.Method);
        }
    }
}
