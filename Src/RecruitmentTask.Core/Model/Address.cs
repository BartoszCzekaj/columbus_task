﻿namespace RecruitmentTask.Core.Model
{
    public sealed class Address
    {
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string City { get; set; }
    }
}
