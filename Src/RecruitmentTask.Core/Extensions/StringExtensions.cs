﻿namespace RecruitmentTask.Core.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string text)
            => !string.IsNullOrWhiteSpace(text);
    }
}
