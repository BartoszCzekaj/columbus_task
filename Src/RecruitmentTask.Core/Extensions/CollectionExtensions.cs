﻿using System.Collections.Generic;
using System.Linq;

namespace RecruitmentTask.Core.Extensions
{
    public static class CollectionExtensions
    {
        public static IEnumerable<T> UseIfNullOrEmpty<T>(this IEnumerable<T> data, IEnumerable<T> replacement = default)
            => data.HasData() ? data : replacement ?? Enumerable.Empty<T>();

        public static bool HasData<T>(this IEnumerable<T> data)
            => data != null && data.Any();
    }
}
