﻿namespace Toolset.Http.Columbus
{
    public class ColumbusHttpClientOptions
    {
        public string EndpointAddress { get; set; }
        public string ApiKey { get; set; }
    }
}