﻿using System.Threading.Tasks;
using Toolset.Http.Columbus.DTO;

namespace Toolset.Http.Columbus.Employees
{
    public interface IEmployeeDetails
    {
        Task<EmployeeDetailsDto> Get(int id);
    }
}
