﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Toolset.Http.Columbus.DTO;

namespace Toolset.Http.Columbus.Employees
{
    public interface IAllEmployees
    {
        Task<IEnumerable<EmployeeDto>> Get(int skip, int take);
        Task<IEnumerable<EmployeeDto>> GetAll();
    }
}
