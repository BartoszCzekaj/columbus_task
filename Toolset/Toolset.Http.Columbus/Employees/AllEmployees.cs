﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Toolset.Http.Columbus.DTO;

namespace Toolset.Http.Columbus.Employees
{
    internal class AllEmployees : IAllEmployees
    {
        private readonly IToolsetHttpColumbusClient _toolsetHttpColumbusClient;
        private readonly ColumbusHttpClientOptions _columbusHttpClientOptions;

        public AllEmployees(ColumbusHttpClientOptions columbusHttpClientOptions
            , IToolsetHttpColumbusClient toolsetHttpColumbusClient)
        {
            _toolsetHttpColumbusClient = toolsetHttpColumbusClient;
            _columbusHttpClientOptions = columbusHttpClientOptions;
        }

        public async Task<IEnumerable<EmployeeDto>> Get(int skip, int take)
        {
            if (skip < 0) { throw new ArgumentException(); }
            if (take < 0) { throw new ArgumentException(); }

            var url = $"{_columbusHttpClientOptions.EndpointAddress}?skip={skip}&take={take}";

            return await _toolsetHttpColumbusClient.GetAsync<IEnumerable<EmployeeDto>>(url).ConfigureAwait(false)
                ?? Enumerable.Empty<EmployeeDto>();
        }

        public async Task<IEnumerable<EmployeeDto>> GetAll()
        {
            
            var url = $"{_columbusHttpClientOptions.EndpointAddress}";

            return await _toolsetHttpColumbusClient.GetAsync<IEnumerable<EmployeeDto>>(url).ConfigureAwait(false)
                ?? Enumerable.Empty<EmployeeDto>();
        }
    }
}
