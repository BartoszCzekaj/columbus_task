﻿using System;
using System.Threading.Tasks;
using Toolset.Http.Columbus.DTO;

namespace Toolset.Http.Columbus.Employees
{
    internal class EmployeeDetails : IEmployeeDetails
    {
        private readonly IToolsetHttpColumbusClient _toolsetHttpColumbusClient;
        private readonly ColumbusHttpClientOptions _columbusHttpClientOptions;

        public EmployeeDetails(ColumbusHttpClientOptions columbusHttpClientOptions
            , IToolsetHttpColumbusClient toolsetHttpColumbusClient)
        {
            _toolsetHttpColumbusClient = toolsetHttpColumbusClient;
            _columbusHttpClientOptions = columbusHttpClientOptions;
        }

        public async Task<EmployeeDetailsDto> Get(int id)
        {
            if (id < 0) { throw new ArgumentException(); }

            var url = $"{_columbusHttpClientOptions.EndpointAddress}/{id}";

            return await _toolsetHttpColumbusClient.GetAsync<EmployeeDetailsDto>(url).ConfigureAwait(false);
        }
    }
}
