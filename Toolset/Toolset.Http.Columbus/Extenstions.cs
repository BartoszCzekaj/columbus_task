﻿using Autofac;
using Polly;
using System;
using System.Net.Http;
using Toolset.Core;
using Toolset.Http.Columbus.Employees;

namespace Toolset.Http.Columbus
{
    public static class Extenstions
    {
        public static IToolsetBuilder AddColumbusHttp(this IToolsetBuilder builder
            , string sectionName = "Columbus"
            )
        {
            var httpColumbusClientOptions = builder.GetOptions<ColumbusHttpClientOptions>(sectionName);
            builder.ContainerBuilder.RegisterInstance(httpColumbusClientOptions).SingleInstance();

            builder.ContainerBuilder.RegisterType<EmployeeDetails>().As<IEmployeeDetails>().InstancePerLifetimeScope();
            builder.ContainerBuilder.RegisterType<AllEmployees>().As<IAllEmployees>().InstancePerLifetimeScope();
            builder.ContainerBuilder.RegisterType<ToolsetHttpColumbusClient>().As<IToolsetHttpColumbusClient>().InstancePerLifetimeScope();
            return builder;
        }
    }
}
