﻿using Polly;
using System;
using System.Net.Http;

namespace Toolset.Http.Columbus
{
    public class ToolsetHttpColumbusClient : ToolsetHttpClient, IToolsetHttpColumbusClient
    {
        public ToolsetHttpColumbusClient(HttpClient client
            , ColumbusHttpClientOptions columbusHttpClientOptions
            , HttpClientOptions httpClientOptions) : base(client, httpClientOptions)
        {
            client.DefaultRequestHeaders.Add("ApiKey", columbusHttpClientOptions.ApiKey);
        }
    }
}
