﻿using System.Collections.Generic;

namespace Toolset.Http.Columbus.DTO
{
    public sealed class EmployeeDto
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public List<LinkDto> Links { get; set; }
    }
}
