﻿namespace Toolset.Http.Columbus.DTO
{
    public sealed class LinkDto
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public string Method { get; set; }
    }
}
