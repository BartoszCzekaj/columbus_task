﻿namespace Toolset.Http.Columbus.DTO
{
    public sealed class AddressDto
    {
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string City { get; set; }
    }
}
