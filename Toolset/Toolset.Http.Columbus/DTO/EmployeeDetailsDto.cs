﻿using System;
using System.Collections.Generic;

namespace Toolset.Http.Columbus.DTO
{
    public sealed class EmployeeDetailsDto
    {
        public DateTime DayOfEmployment { get; set; }
        public string PhoneNumber { get; set; }
        public string JobTitle { get; set; }
        public double Salary { get; set; }
        public string Email { get; set; }
        public List<LinkDto> Links { get; set; }
        public AddressDto Address { get; set; }
    }
}
