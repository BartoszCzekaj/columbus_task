﻿using Autofac;
using System.Net.Http;
using Toolset.Core;

namespace Toolset.Http
{
    public static class Extensions
    {
        public static IToolsetBuilder AddHttpClient(this IToolsetBuilder builder
           , string httpClientSectionName = "httpClient")
        {
            var httpClientOptions = builder.GetOptions<HttpClientOptions>(httpClientSectionName);

            builder.ContainerBuilder.RegisterInstance(httpClientOptions).SingleInstance();
            builder.ContainerBuilder.Register(c => new HttpClient()).As<HttpClient>(); 
            builder.ContainerBuilder.RegisterType<ToolsetHttpClient>().As<IToolsetHttpClient>().InstancePerLifetimeScope();
            return builder;
        }
    }
}
