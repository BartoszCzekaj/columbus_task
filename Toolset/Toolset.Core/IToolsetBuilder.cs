﻿using Autofac;
using System.Collections.Specialized;

namespace Toolset.Core
{
    public interface IToolsetBuilder
    {
        ContainerBuilder ContainerBuilder { get; }
        NameValueCollection Settings { get; }
    }
}
