﻿using Autofac;
using System.Collections.Specialized;

namespace Toolset.Core
{
    public class ToolsetBuilder : IToolsetBuilder
    {
        public ContainerBuilder ContainerBuilder { get; }
        public NameValueCollection Settings { get; }

        private ToolsetBuilder(ContainerBuilder containerBuilder, NameValueCollection settings)
            => (ContainerBuilder, Settings) = (containerBuilder, settings);

        public static IToolsetBuilder Create(ContainerBuilder containerBuilder, NameValueCollection settings)
            => new ToolsetBuilder(containerBuilder, settings);
    }
}
