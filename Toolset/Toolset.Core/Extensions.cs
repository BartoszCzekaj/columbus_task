﻿using Autofac;
using Newtonsoft.Json;
using System.Collections.Specialized;

namespace Toolset.Core
{
    public static class Extensions
    {
        public static IToolsetBuilder AddToolset(this ContainerBuilder configurationBuilder, NameValueCollection settings)
            => ToolsetBuilder.Create(configurationBuilder, settings);

        public static TModel GetOptions<TModel>(this IToolsetBuilder builder, string settingsSectionName)
           where TModel : new()
        {
            var sectionData = builder.Settings[settingsSectionName];
            var model = (TModel)JsonConvert.DeserializeObject(sectionData, typeof(TModel));
          
            return model;
        }

       
    }
}
